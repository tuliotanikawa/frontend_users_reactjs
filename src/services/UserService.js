import axios from 'axios';

class UserService {
    _urlBase = `http://localhost:8080/api/users`;

    _axiosInstance = axios.create({
        baseURL: this._urlBase
    });

    save = (user) => {
        const userStringify = JSON.stringify(user)
        const _url = `${this._urlBase}`;
        return this._axiosInstance.post(_url, userStringify, { json: true, headers: { 'Content-Type': 'application/json' } });
    }

    findById = (id) => {
        const _url = `${this._urlBase}/${id}`;
        return this._axiosInstance.get(_url).then(response => {
            return response.data
        }).catch(error => {
            console.log(error);
        });
    }

    findAll = (fieldName, fieldValue) => {
        let _url;
        if (fieldName !== 'todos')
            _url = `${this._urlBase}/?${fieldName}=${fieldValue}`;
        else
            _url = `${this._urlBase}/`;
        return this._axiosInstance.get(_url).then(response => {
            return response.data
        }).catch(error => {
            console.log(error);
        });
    }

    deleteUserById = (id) => {
        const _url = `${this._urlBase}/${id}`;
        return this._axiosInstance.delete(_url).then(response => {
            return response.data
        }
        ).catch(error => {
            console.log(error);
        });
    }

}
export default UserService;