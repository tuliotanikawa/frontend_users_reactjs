import { observable, action } from 'mobx';
//Model
import UserModel from '../models/UserModel';
//Service
import UserService from '../services/UserService';

export default class UserStore {

  @observable user = new UserModel();
  @observable users = []
  userService = new UserService();

  @action
  setUser = userById => {
    this.user.setValue('id', userById.id);
    this.user.setValue('username', userById.username);
    this.user.setValue('password', userById.password);
    this.user.setValue('name', userById.name);
    this.user.setValue('surname', userById.surname);
    this.user.setValue('is_enabled', userById.is_enabled);
    this.user.setValue('phone', userById.phone);
    this.user.setValue('email', userById.email);
    this.user.setValue('register_date', userById.register_date);
  }

  save = () => {
    return this.userService.save(this.user).then(() => this.reset())
  }
  findById = (id) => {
    return this.userService.findById(id).then((userById) => this.setUser(userById))
  }

  findAll = (fieldName, fieldValue) => {
    return this.userService.findAll(fieldName, fieldValue).then(users => {
      this.users = users
    })
  }

  deleteUserById = (id) => {
    return this.userService.deleteUserById(id).then(response => { return this.users = this.users.filter(user => user.id !== id) });
  }

  reset = () => {
    this.user = new UserModel();
  }

}
