import React, { Component } from 'react'
import { observer } from 'mobx-react'
import UserStore from '../../stores/UserStore'
import FormUsers from './FormUsers'
import { notification } from 'antd';
import 'antd/dist/antd.css';  // or 'antd/dist/antd.less'
import queryString from 'query-string';

@observer
class FormUsersController extends Component {

    componentWillMount() {
        if (this.props.location.search)
            this.userStore.findById(queryString.parse(this.props.location.search).id)
    }

    userStore = new UserStore()

    setValueUser = (name, value) => {
        this.userStore.user.setValue(name, value);
    }

    save = () => {
        return this.userStore.save().then(() => !undefined ? this.mensagemSucesso('Cadastro realizado com Sucesso') : undefined)
            .catch((erro) => {
                this.mensagemErro(erro.message)
                this.setState({ visibleError: true })
            })
    }

    mensagemSucesso = (message) => notification['success']({ message, duration: 3 });

    mensagemErro = (message) => notification['error']({ message, duration: 0, placement: 'bottomRight', description: 'Oops! Verifique os dados, sua conexão com a Internet e tente novamente' });

    render() {

        return (
            <div className='FormUsersController'>
                <FormUsers
                    save={this.save}
                    setValueUser={this.setValueUser}
                    model={this.userStore.user}
                />
            </div>

        )
    }
}
export default FormUsersController;
