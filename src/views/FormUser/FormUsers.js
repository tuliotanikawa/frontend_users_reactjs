import React, { Component } from 'react';
import { Divider } from 'semantic-ui-react';
import { Form } from 'formsy-semantic-ui-react'
import { observer } from 'mobx-react';
import propTypes from 'prop-types';
import UserModel from '../../models/UserModel';

@observer
export default class FormUsers extends Component {

 static propTypes = {
    model: propTypes.instanceOf(UserModel).isRequired,
    save: propTypes.func.isRequired,
    setValueUser: propTypes.func.isRequired,

 }

    _setValueUser = (e, { name, value }) => this.props.setValueUser(name, value.trim())

    save = () => {
        this.props.save().then(() => {
            this.form.formsyForm.reset()
        });
    }

    isDisabled = () => {
        const { model } = this.props;
        return model.email && model.name && model.phone && model.surname && model.username && model.password ? false : true;
    }


    render() {

        return (
            <Form onSubmit={this.save} ref={form => this.form = form} >
                <Divider horizontal >New User</Divider>
                <Form.Group widths='equal'>
                    <Form.Input label='Username' name='username' placeholder='Username' value={this.props.model.username} required
                        onChange={this._setValueUser} validationErrors={{
                            isDefaultRequiredValue: 'Preenchimento obrigatório.',
                        }} errorLabel={<label />} />

                    <Form.Input label='Email' name='email' placeholder='Email' value={this.props.model.email} onChange={this._setValueUser}
                        validations='isEmail'
                        validationErrors={{ isEmail: 'Não é um valor válido para e-mail.' }}
                        errorLabel={<label />} />
                    <Form.Input label='Password' name='password' placeholder='Password' value={this.props.model.password} required
                        onChange={this._setValueUser} validationErrors={{
                            isDefaultRequiredValue: 'Preenchimento obrigatório.',
                        }} errorLabel={<label />} />
                </Form.Group>
                <Divider horizontal >Additional Data</Divider>
                <Form.Input label='Name' name='name' placeholder='Name' value={this.props.model.name} required
                    onChange={this._setValueUser} validationErrors={{
                        isDefaultRequiredValue: 'Preenchimento obrigatório.',
                    }} errorLabel={<label />} />
                <Form.Input label='Surname' name='surname' placeholder='Surname' value={this.props.model.surname} required
                    onChange={this._setValueUser} validationErrors={{
                        isDefaultRequiredValue: 'Preenchimento obrigatório.',
                    }} errorLabel={<label />} />

                <Form.Input type="Number" label='Phone' name='phone' placeholder='Phone number' required value={this.props.model.phone}
                    onChange={this._setValueUser} validationErrors={{
                        isNumeric: 'Preenchimento obrigatório.',
                    }} errorLabel={<label />} />

                <Form.Button disabled={this.isDisabled() ? true : false} positive >Salvar</Form.Button>
            </Form>
        );
    }
}

