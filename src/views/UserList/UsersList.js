import React, { Component } from 'react';
import { Input, Select, Button, Divider } from 'semantic-ui-react';
import { observer } from 'mobx-react';
import { observable, action } from 'mobx';

@observer
class UsersList extends Component {
    @observable
    fieldName = "todos";
    @observable
    fieldValue = "";

    @action
    setFieldName = fieldName => this.fieldName = fieldName;
    @action
    setFieldValue = fieldValue => this.fieldValue = fieldValue;

    findAll = () => {
        this.props.findAll(this.fieldName, this.fieldValue)
    }
    _setFieldValue = (e, { name, value }) => {
        this.setFieldValue(value);
    }
    _setFieldName = (e, { name, value }) => {
        this.setFieldName(value);
    }

    render() {
        const options = [
            { key: 'name', text: 'Name', value: 'name' },
            { key: 'username', text: 'Username', value: 'username' },
            { key: 'email', text: 'E-mail', value: 'email' },
            { key: 'todos', text: 'Trazer Todos', value: 'todos' },
        ]
        return (
            <div>
                <Input name='searchValues' type='text' placeholder='Search...' action onChange={this._setFieldValue}>
                    <input disabled={this.fieldName === "todos"} value={this.fieldName === "todos" ? "" : this.fieldValue} />
                    <Select compact options={options} name='fieldName' defaultValue='todos' onChange={this._setFieldName} />
                    <Button onClick={this.findAll} >Search</Button>
                </Input>
                <Divider hidden />
            </div>
        );
    }
}

export default UsersList;
