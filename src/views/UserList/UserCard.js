import React, { Component } from 'react';
import { NavLink } from "react-router-dom";
import {Icon} from "semantic-ui-react";
import {observer} from "mobx-react";
@observer
class UserCard extends Component {

  geraRandom(min, max) {
    return Math.floor(Math.random() * (max - min + 1) + min);
  }
  
  deleteUserById= () =>{
      this.props.deleteUserById(this.props.user.id);
  }
  render() {

    const user = this.props.user

    const avatars = ['https://semantic-ui.com/images/avatar2/large/kristy.png',
      'https://semantic-ui.com/images/avatar2/large/matthew.png',
      'https://semantic-ui.com/images/avatar2/large/molly.png',
      'https://semantic-ui.com/images/avatar2/large/elyse.png',
      'https://semantic-ui.com/images/avatar/large/steve.jpg']

    return (
      <div className='ui card'>
        <img alt='Imagem de avatar' src={avatars[this.geraRandom(0, 4)]} className='ui image' />
        <div className='content'>
          <div className='header'>{user.nome}</div>
          <div className='meta'>
            <span className='date'>Id: {user.id}</span>
          </div>
          <div className='meta'>
            <span className='date'>Name: {user.name}</span>
          </div>
          <div className='meta'>
            <span className='date'>Email: {user.email}</span>
          </div>
          <div className='meta'>
            <span className='date'>Username: {user.username}</span>
          </div>
          <div className='meta'>
            <span className='date' style={{wordWrap: 'break-word'}}>Password (hash): {user.password}</span>
          </div>
        </div>
        <div className='extra content'>
            Surname: {user.surname}<br />
            Phone: {user.phone} <br />
            Is_Enabled: {user.is_enabled? 'true': 'false'} <br />
            Register Date: {user.register_date} <br />
            <Icon link onClick={this.deleteUserById} name='trash' /> 
            <NavLink to={`/FormUsers?id=${user.id}`} name="Cadastrar" className="item"  ><i aria-hidden='true' className='pencil icon'></i>  </NavLink>
        </div>
      </div>
    )
  }
}
export default UserCard