import React, { Component } from 'react'
import { observer } from 'mobx-react'
import UserStore from '../../stores/UserStore'
import UsersList from './UsersList'
import { Container } from 'semantic-ui-react';
import UserCard from './UserCard'

@observer
class UserListController extends Component {

    userStore = new UserStore()

    componentDidMount() {
        return this.userStore.findAll("todos", null);
    }
    findAll = (fieldName, fieldValue) => {
        return this.userStore.findAll(fieldName, fieldValue);
    }

    deleteUserById = (id) => {
        return this.userStore.deleteUserById(id);
    }

    render() {
        return (
            <div className='UserListController'>
                <div className='container'>
                    <UsersList
                        findAll={this.findAll} />
                </div>
                <div>{
                    <Container textAlign='left'>
                        <div className="ui four cards" >
                            {this.userStore.users && this.userStore.users.map((user) => <UserCard key={user.id} user={user} deleteUserById={this.deleteUserById} />)}  </div>
                    </Container>
                }</div>

            </div>
        )
    }
}
export default UserListController;
