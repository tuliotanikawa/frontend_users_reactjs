import React, { Component } from 'react'
import propTypes from 'prop-types';
import 'antd/dist/antd.css';  // or 'antd/dist/antd.less'

class FizzBuzzList extends Component {
    static propTypes = {
        list : propTypes.array.isRequired,
    }
    render() {

        return (
            <div className='FormUsersController'>
                <ul>
                    {this.props.list.map((element, i) => <li key={i}>{element}</li>)}
                </ul>
            </div>

        )
    }
}
export default FizzBuzzList;