import React, { Component } from 'react'
import FizzBuzzList from './FizzBuzzList'
import 'antd/dist/antd.css';  // or 'antd/dist/antd.less'

class FizzBuzzController extends Component {

    fizzBuzz = () => {
        const renderizar = [];
        for (let i = 1; i <= 100; i++) {
            if (i % 3 === 0) {
                if (i % 5 === 0) {
                    renderizar.push("fizzbuzz");
                } else {
                    renderizar.push("fizz");
                }
            } else if (i % 5 === 0) {
                renderizar.push("buzz");
            } else {
                renderizar.push(i);
            }
        }
        return renderizar;
    }

    render() {

        return (
            <div className='FormUsersController'>
                <FizzBuzzList list={this.fizzBuzz()} />
            </div>

        )
    }
}
export default FizzBuzzController;