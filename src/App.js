import React, { Component } from 'react';
import { Segment } from 'semantic-ui-react';
import HeaderExemplo from './views/HeaderExemplo';
import UserListController from './views/UserList/UserListController';
import FormUsersController from './views/FormUser/FormUsersController';
import FizzBuzzController from './views/FizzBuzz/FizzBuzzController';
import { Route,NavLink,BrowserRouter } from "react-router-dom";
import './App.css';

class App extends Component {

  render() {

    return (
      <div className="App">
        <div className="ui container">
          <Segment>
            <HeaderExemplo />
            <BrowserRouter>
              <div>
                <div className="ui menu">
                  <NavLink to="/UsersList" name="lista" className="item" >Listar Users</NavLink>
                  <NavLink to="/FormUsers" name="Cadastrar" className="item"  >Cadastrar</NavLink>
                  <NavLink to="/FizzBuzz" name="fizzbuzz" className="item" >FizzBuzz</NavLink>
                </div>
                <div className="content">
                  <Segment>
                    <Route path="/UsersList" component={UserListController} />
                    <Route path="/FormUsers" component={FormUsersController} />
                    <Route path="/FizzBuzz" component={FizzBuzzController} />
                  </Segment>
                </div>
              </div>
            </BrowserRouter>
          </Segment>
        </div>
      </div>
    );
  }
}

export default App;
