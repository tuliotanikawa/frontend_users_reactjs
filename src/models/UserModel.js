import { observable, action } from 'mobx';
export default class UserModel {

  @observable id = undefined;
  @observable username = undefined;
  @observable password = undefined;
  @observable name = undefined;
  @observable surname = undefined;
  @observable is_enabled = undefined;
  @observable email = undefined;
  @observable phone = undefined;
  @observable register_date = undefined;
  
  @action
  setValue = (name, value) => {
      this[name] = value;
  }
}
